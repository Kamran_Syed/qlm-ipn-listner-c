/*************************************************************************
 * 
 * SORACO TECHNOLOGIES
 * __________________
 * 
 *  [2002] - [2015] Soraco Technologies Inc.
 *  All Rights Reserved.
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of Soraco Technologies Inc. and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Soraco Technologies Inc.
 * and its suppliers and may be covered by Canadian and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Soraco Technologies Inc..
 */

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Xml;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.IO;
using System.Reflection;
using System.Collections;

using QlmSvcLib.EventLogDsTableAdapters;
using QlmPaypalIpn.Properties;
using System.Collections.Generic;
using QlmSvcLib;
using QlmSvcLib.ServerPropertiesDsTableAdapters;
using System.Globalization;


//
// Events Next Index: 245
//

namespace QlmPaypalIpn
{
    public class QlmIpn
    {
        const string THIS_MODULE = "QlmPaypalIpn";

        string paypalUrl = "https://www.sandbox.paypal.com/cgi-bin/webscr";
        string qlmWebUrl = "http://quicklicensemanager.com/qlmdemov8/qlmservice.asmx";
        string companyName = "Soraco Technologies";
        string supportEmail = "support@soraco.co";
        string qlmVersion = "5.0.00";

        private int loggingLevel = 3;
        public enum EventType { Error = 1, Warning = 2, Information = 4, Verbose = 8 };

        static public bool refreshServerProperties = true;
        static public QlmSvcLib.ServerPropertiesDs.ServerPropertiesDataTable serverPropertiesDataTable = null;



        #region Public Methods
        public QlmIpn ()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        }
        /// <summary>
        /// Only public method, start processing transaction here.
        /// </summary>
        /// <param name="context">Http context, assume the paypal IPN</param>
        public void ProcessTransaction(HttpContext context)
        {
            if (context == null)
            {
                return;
            }

            //Get the name of the user calling
            string callingUser = CallingUser(context);

            try
            {

                LogEvent("QlmPaypalIpn", 10, EventType.Information, "ProcessTransaction", callingUser, "Start");

                InitializeSettings(context);

                OrderInfo orderInfo = new OrderInfo ();
                orderInfo.CallingUser = callingUser;

                //Initialize storage values for paypal arguments
                string productId = string.Empty;
                string major = string.Empty;
                string minor = string.Empty;
                List<ItemData> allItems = new List<ItemData>(); //Modified to a list of custom argument strings. Will collect only 1 if single item or multiple if many items

                //Set values from the IPN context
                //Standard (always single) values
                orderInfo.PaymentStatus = RequestValue(context, "payment_status");
                orderInfo.PayerEmail = RequestValue(context, "payer_email");
                orderInfo.Business = RequestValue(context, "business");
                orderInfo.TransactionType = RequestValue(context, "txn_type");

                //Possible multi-item variables, check if we have an 'item_number1' which will indicate cart system
                //must declare custom arg variables and quantity outside the if statement but don't bother setting until
                //we know what should be checked
                string customArg = string.Empty;
                string quantity = string.Empty;
                string itemName = string.Empty;
                string multiCheck = RequestValue(context, "item_number1");

                if (string.IsNullOrEmpty(multiCheck))
                {
                    //No item 1, process as single item transaction
                    LogEvent("QlmPaypalIpn", 155, EventType.Information, "ProcessTransaction", callingUser, "Parse Single Item");
                    customArg = GetProductArgument(context, -1);
                    
                    quantity = RequestValue(context, "quantity");
                    itemName = RequestValue(context, "item_name");

                    //see if we have a valid quantity
                    if (String.IsNullOrEmpty(quantity))
                    {
                        //Check for the quantity1 value if quantity does not exist (multiple items)
                        quantity = RequestValue(context, "quantity1"); //This should not exist if item_number1 didn't exist but check anyway
                        if (String.IsNullOrEmpty(quantity))
                        {
                            //Did not find a quantity, default to 1
                            quantity = "1";
                        }
                    }

                    //Check if the qlm specific items were set in the custom field
                    if (String.IsNullOrEmpty(customArg))
                    {
                        //Nothing found, set to default value
                        customArg = Settings.Default.defaultUrlArgs;
                    }
                    

                    //Add the quantity to the qlm specific items
                    customArg += "&is_quantity=" + quantity;

                    //Add this to the list
                    allItems.Add(new ItemData(customArg, 0, itemName));
                }
                else
                {
                    //Has an item number, process as multiple item transaction
                    LogEvent("QlmPaypalIpn", 160, EventType.Information, "ProcessTransaction", callingUser, "Parse multi-item");
                    int curItem = 1;

                    //Loop until we don't have an item number anymore
                    do
                    {
                        //Get the item number value for the current item (custom shouldn't work with multiples I don't think...)
                        customArg = GetProductArgument(context, curItem);
                        

                        itemName = RequestValue(context, "item_name" + curItem);

                        //Check if we got a value, if so process it, if not we are at the end of the loop.
                        if (string.IsNullOrEmpty(customArg))
                        {
                            LogEvent("QlmPaypalIpn", 165, EventType.Information, "ProcessTransaction", callingUser, "Custom arg not found.");
                            break; 
                        }
                        else
                        {
                            LogEvent("QlmPaypalIpn", 165, EventType.Information, "ProcessTransaction", callingUser, "Found custom arg: " + customArg);
                            //Process the string
                            quantity = RequestValue(context, "quantity" + curItem);

                            //Make sure we have a valid quantity
                            if (String.IsNullOrEmpty(quantity))
                            {
                                //Did not find a quantity, default to 1
                                quantity = "1";
                            }

                            //Add the quantity to the qlm specific items
                            customArg += "&is_quantity=" + quantity;

                            //Add this to the list
                            allItems.Add(new ItemData(customArg, curItem, itemName));
                        }

                        //Iteration variable
                        curItem += 1;
                    }
                    while (true);
                }

                //Try to validate the transaction with PayPal
                string response = String.Empty;
                try
                {
                    //Post back to PayPal system to validate
                    response = SendToPaypal(context);
                }
                catch (Exception ex)
                {
                    ReportException(context, ex);
                    LogEvent(THIS_MODULE, 130, EventType.Error, "ProcessTransaction", callingUser, "Exception sending response to Paypal: " + ex.Message);
                }

                //Check if there was a response from PayPal or not first
                if (String.IsNullOrEmpty(response))
                {
                    LogEvent(THIS_MODULE, 25, EventType.Error, "ProcessTransaction", callingUser, "Empty response.");
                }
                else
                {
                    LogEvent(THIS_MODULE, 30, EventType.Information, "ProcessTransaction", callingUser, "Paypal response:" + response);
                }

                //Check what PayPal response was
                switch (response)
                {
                    case "VERIFIED":
                        {
                            //PayPal verified validity, proceed with processing

                            try
                            {
                                // if the order is completed, get an activation key from QlmWeb
                                LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", callingUser, "Payment Status: " + orderInfo.PaymentStatus);
                                LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", callingUser, "Transaction Type: " + orderInfo.TransactionType);

                                if (orderInfo.PaymentStatus == "Completed" || (orderInfo.TransactionType == "subscr_signup"))//New subscription as well as payment
                                {
                                    foreach (ItemData itm in allItems)
                                    {
                                        ProcessItem(context, orderInfo, itm);
                                    }
                                }
                                else if (orderInfo.PaymentStatus == "Refunded")
                                {
                                    // We do not currently handle refunds
                                    return;

                                }
                                else
                                {
                                    LogEvent(THIS_MODULE, 40, EventType.Error, "ProcessTransaction", callingUser, "Order is not complete.");
                                    SendMail(context,
                                                orderInfo.Business, orderInfo.Business, "Order Info",
                                                "Order is not complete. Payment status = " + orderInfo.PaymentStatus);
                                }

                            }
                            catch (System.Net.WebException e)
                            {
                                SendMail(context,
                                            orderInfo.Business, orderInfo.PayerEmail,
                                            "Your purchase with " + CompanyName,
                                            "Cannot retrieve your license key. Please contact " + CompanyName + "at " + SupportEmail + ".");

                                ReportException(context, e);
                                return;
                            }

                            // and send the key to the customer by email
                            try
                            {
                                SendCustomerActivationKey(context, allItems);
                            }
                            catch (System.Exception e)
                            {
                                ReportException(context, e);
                                LogEvent(THIS_MODULE, 50, EventType.Error, "ProcessTransaction", callingUser, "Exception: " + e.Message);
                                return;
                            }
                            break;
                        }
                    default:
                        {
                            // Possible fraud. Log for investigation.
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                context.Response.Write(ex.Message + ex.StackTrace);
                LogEvent(THIS_MODULE, 140, EventType.Error, "ProcessTransaction", callingUser, "Exception: " + ex.Message);
            }

        }

#if OLD
        public void ProcessTransaction(HttpContext context)
        {
            if (context == null)
            {
                return;
            }

            //Get the name of the user calling
            string callingUser = CallingUser(context);

            try
            {

                LogEvent("QlmPaypalIpn", 10, EventType.Information, "ProcessTransaction", callingUser, "Start");

                InitializeSettings(context);

                OrderInfo orderInfo = new OrderInfo();
                orderInfo.CallingUser = callingUser;

                //Initialize storage values for paypal arguments
                string productId = string.Empty;
                string major = string.Empty;
                string minor = string.Empty;
                List<ItemData> allItems = new List<ItemData>(); //Modified to a list of custom argument strings. Will collect only 1 if single item or multiple if many items

                //Set values from the IPN context
                //Standard (always single) values
                orderInfo.PaymentStatus = RequestValue(context, "payment_status");
                orderInfo.PayerEmail = RequestValue(context, "payer_email");
                orderInfo.Business = RequestValue(context, "business");

                //Possible multi-item variables, check if we have an 'item_number1' which will indicate cart system
                //must declare custom arg variables and quantity outside the if statement but don't bother setting until
                //we know what should be checked
                string customArg = string.Empty;
                string quantity = string.Empty;
                string itemName = string.Empty;
                string multiCheck = RequestValue(context, "item_number1");

                if (string.IsNullOrEmpty(multiCheck))
                {
                    //No item 1, process as single item transaction
                    LogEvent("QlmPaypalIpn", 155, EventType.Information, "ProcessTransaction", callingUser, "Parse Single Item");
                    customArg = GetProductArgument(context, -1);

                    quantity = RequestValue(context, "quantity");
                    itemName = RequestValue(context, "item_name");

                    //see if we have a valid quantity
                    if (String.IsNullOrEmpty(quantity))
                    {
                        //Check for the quantity1 value if quantity does not exist (multiple items)
                        quantity = RequestValue(context, "quantity1"); //This should not exist if item_number1 didn't exist but check anyway
                        if (String.IsNullOrEmpty(quantity))
                        {
                            //Did not find a quantity, default to 1
                            quantity = "1";
                        }
                    }

                    //Check if the qlm specific items were set in the custom field
                    if (String.IsNullOrEmpty(customArg))
                    {
                        //Nothing found, set to default value
                        customArg = Settings.Default.defaultUrlArgs;
                    }


                    //Add the quantity to the qlm specific items
                    customArg += "&is_quantity=" + quantity;

                    //Add this to the list
                    allItems.Add(new ItemData(customArg, 0, itemName));
                }
                else
                {
                    //Has an item number, process as multiple item transaction
                    LogEvent("QlmPaypalIpn", 160, EventType.Information, "ProcessTransaction", callingUser, "Parse multi-item");
                    int curItem = 1;

                    //Loop until we don't have an item number anymore
                    do
                    {
                        //Get the item number value for the current item (custom shouldn't work with multiples I don't think...)
                        customArg = GetProductArgument(context, curItem);


                        itemName = RequestValue(context, "item_name" + curItem);

                        //Check if we got a value, if so process it, if not we are at the end of the loop.
                        if (string.IsNullOrEmpty(customArg))
                        {
                            LogEvent("QlmPaypalIpn", 165, EventType.Information, "ProcessTransaction", callingUser, "Custom arg not found.");
                            break;
                        }
                        else
                        {
                            LogEvent("QlmPaypalIpn", 165, EventType.Information, "ProcessTransaction", callingUser, "Found custom arg: " + customArg);
                            //Process the string
                            quantity = RequestValue(context, "quantity" + curItem);

                            //Make sure we have a valid quantity
                            if (String.IsNullOrEmpty(quantity))
                            {
                                //Did not find a quantity, default to 1
                                quantity = "1";
                            }

                            //Add the quantity to the qlm specific items
                            customArg += "&is_quantity=" + quantity;

                            //Add this to the list
                            allItems.Add(new ItemData(customArg, curItem, itemName));
                        }

                        //Iteration variable
                        curItem += 1;
                    }
                    while (true);
                }

                //Try to validate the transaction with PayPal
                string response = String.Empty;
                try
                {
                    //Post back to PayPal system to validate
                    response = SendToPaypal(context);
                }
                catch (Exception ex)
                {
                    ReportException(context, ex);
                    LogEvent(THIS_MODULE, 130, EventType.Error, "ProcessTransaction", callingUser, "Exception sending response to Paypal: " + ex.Message);
                }

                //Check if there was a response from PayPal or not first
                if (String.IsNullOrEmpty(response))
                {
                    LogEvent(THIS_MODULE, 25, EventType.Error, "ProcessTransaction", callingUser, "Empty response.");
                }
                else
                {
                    LogEvent(THIS_MODULE, 30, EventType.Information, "ProcessTransaction", callingUser, "Paypal response:" + response);
                }

                //Check what PayPal response was
                switch (response)
                {
                    case "VERIFIED":
                        {
                            //PayPal verified validity, proceed with processing



                            //Iterate items
                            foreach (ItemData itm in allItems)
                            {


                                //Parse the string (constructs a hash table of values for this item which can then be passed to emails, etc.)
                                Hashtable argsTable;
                                ParseQueryString(context, itm.UrlArgs, out argsTable);
                                itm.ArgsTable = argsTable;

                                try
                                {
                                    // if the order is completed, get an activation key from QlmWeb
                                    LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", callingUser, "Payment Status: " + orderInfo.PaymentStatus);
                                    if (orderInfo.PaymentStatus == "Completed")
                                    {
                                        response = SendToQlmWebService(context, "paypal", itm.UrlArgs);

                                    }
                                    else if (orderInfo.PaymentStatus == "Refunded")
                                    {
                                        // We do not currently handle refunds
                                        return;

                                    }
                                    else
                                    {
                                        LogEvent(THIS_MODULE, 40, EventType.Error, "ProcessTransaction", callingUser, "Order is not complete.");
                                        SendMail(context,
                                                 orderInfo.Business, orderInfo.Business, "Order Info",
                                                 "Order is not complete. Payment status = " + orderInfo.PaymentStatus);
                                    }

                                }
                                catch (System.Net.WebException e)
                                {
                                    SendMail(context,
                                             orderInfo.Business, orderInfo.PayerEmail,
                                             "Your purchase with " + CompanyName,
                                             "Cannot retrieve your license key. Please contact " + CompanyName + "at " + SupportEmail + ".");

                                    ReportException(context, e);
                                    return;
                                }

                                // parse the activation key
                                string keys = string.Empty;
                                XmlDocument xmlDoc = new XmlDocument();

                                try
                                {
                                    xmlDoc.LoadXml(response);

                                    System.Xml.XmlNodeList nodes = xmlDoc.GetElementsByTagName("Key");
                                    if (nodes.Count != 0)
                                    {
                                        foreach (System.Xml.XmlNode node in nodes)
                                        {
                                            keys += node.InnerText + "\n";
                                        }
                                    }
                                }
                                catch (System.Exception e)
                                {
                                    SendMail(context,
                                               orderInfo.Business, orderInfo.PayerEmail,
                                               "Your purchase with " + CompanyName,
                                               "Cannot retrieve your license key. Please contact " + CompanyName + "at " + SupportEmail + ".");

                                    ReportException(context, e);
                                    LogEvent(THIS_MODULE, 45, EventType.Error, "ProcessTransaction", callingUser, "Exception: " + e.Message);
                                    return;
                                }

                                //Collect the data to be put in the email
                                itm.ActivationKeys = keys;
                            }

                            // and send the key to the customer by email
                            try
                            {
                                SendCustomerActivationKey(context, allItems);
                            }
                            catch (System.Exception e)
                            {
                                ReportException(context, e);
                                LogEvent(THIS_MODULE, 50, EventType.Error, "ProcessTransaction", callingUser, "Exception: " + e.Message);
                                return;
                            }
                            break;
                        }
                    default:
                        {
                            // Possible fraud. Log for investigation.
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                context.Response.Write(ex.Message + ex.StackTrace);
                LogEvent(THIS_MODULE, 140, EventType.Error, "ProcessTransaction", callingUser, "Exception: " + ex.Message);
            }

        }
#endif
        #endregion

        #region Private Methods
        /// <summary>
        /// Initializes the settings, sets logging level, and sets properties from default settings.
        /// </summary>
        /// <param name="context">http context for this request.</param>
        private void InitializeSettings(HttpContext context)
        {
            try
            {

                loggingLevel = Settings.Default.loggingLevel;
                paypalUrl = Settings.Default.paypalUrl;
                qlmWebUrl = Settings.Default.qlmWebServiceUrl;
                CompanyName = Settings.Default.vendorCompanyName;
                SupportEmail = Settings.Default.vendorCompanyEmail;
                QlmVersion = Settings.Default.qlmVersion;

                InitServerProperties();

                loggingLevel = GetServerProperty("paypalLoggingLevel", Settings.Default.loggingLevel);
                paypalUrl = GetServerProperty("paypalUrl", Settings.Default.paypalUrl);
                qlmWebUrl = GetServerProperty("qlmWebServiceUrl", Settings.Default.qlmWebServiceUrl);
                CompanyName = GetServerProperty("vendorCompanyName", Settings.Default.vendorCompanyName);
                SupportEmail = GetServerProperty("vendorCompanyEmail", Settings.Default.vendorCompanyEmail);
                QlmVersion = GetServerProperty("qlmVersion", Settings.Default.qlmVersion);



                LogEvent("QlmPaypalIpn", 120, EventType.Information, "InitializeSettings", CallingUser(context),
                    String.Format("Start: paypalUrl: {0}, qlmUrl: {1}", paypalUrl, qlmWebUrl));
            }
            catch
            { }

        }

        private void SendMail(HttpContext context, string from, string to, string subject, string body)
        {
            LogEvent(THIS_MODULE, 75, EventType.Information, "SendMail", CallingUser(context), "Start");

            try
            {
                SmtpClient mail = new SmtpClient();

                mail.Host = GetServerProperty("smtpServer", Settings.Default.smtpServer);
                mail.Port = GetServerProperty("smtpPort", Settings.Default.smtpPort);

                if (GetServerProperty ("smtpEnableSSL", Settings.Default.smtpEnableSSL))
                {
                    mail.EnableSsl = true;
                }

                string smtpuser = GetServerProperty("smtpUser", Settings.Default.smtpUser);


                NetworkCredential nc = new NetworkCredential(smtpuser, GetServerProperty("smtpPassword", Settings.Default.smptPassword));
                mail.UseDefaultCredentials = false;
                mail.Credentials = nc;

                MailMessage mailMessage = new MailMessage();

                if (Settings.Default.htmlFormat)
                {
                    mailMessage.IsBodyHtml = true;
                }

                MailAddress fromMailAddress = new MailAddress(from);
                mailMessage.From = fromMailAddress;

                char[] mail_separators = new char[] { ';', ',' };

                string[] recipients = to.Split(mail_separators, StringSplitOptions.RemoveEmptyEntries);
                foreach (string recipient in recipients)
                {
                    mailMessage.To.Add(recipient);
                }

                mailMessage.CC.Add(fromMailAddress);

                mailMessage.Subject = subject;
                mailMessage.Body = body;

                string msg = String.Format("Sending mail to: {0} from {1} using SMTP server {2} with account {3}", to, from, mail.Host, smtpuser);
                LogEvent(THIS_MODULE, 70, EventType.Information, "SendMail", CallingUser(context), msg);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;            
                mail.Send(mailMessage);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;            
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 65, EventType.Error, "SendMail", CallingUser(context), ex.Message);
                ReportException(context, ex);
            }
        }

        private void ReportException(HttpContext context, Exception ex)
        {
            context.Response.Write(ex.Message + "\r\n");

        }

        // This helper method encodes a string correctly for an HTTP POST
        private string Encode(string oldValue)
        {
            string newValue = oldValue.Replace("\"", "'");
            newValue = System.Web.HttpUtility.UrlEncode(newValue);
            newValue = newValue.Replace("%2f", "/");
            return newValue;
        }

        /// <summary>
        /// Parses the custom query string for QLM passed by the IPN.  Returns bool indicating success or failure.
        /// UrlArgs are of the form: &is_productid=1&is_majorversion=1&is_minorversion=0&is_features=0
        /// </summary>
        /// <param name="context">http context of the request</param>
        /// <param name="urlArgs">The fully built arguments string.</param>
        /// <returns></returns>
        private bool ParseQueryString(HttpContext context, string UrlArgs, out Hashtable argsTable)
        {
            argsTable = new Hashtable();
            bool ret = false;

            try
            {
                LogEvent(THIS_MODULE, 115, EventType.Information, "ParseQueryString", CallingUser(context), "Start parsing: " + UrlArgs);

                // Support any of the following separators
                string[] sep = { "&", "|" };

                string[] args = UrlArgs.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                foreach (string arg in args)
                {
                    string[] sep1 = { "=" };
                    string[] propValue = arg.Split(sep1, StringSplitOptions.RemoveEmptyEntries);

                    if (propValue.Length > 1)
                    {
                        string left = propValue[0].Trim();
                        string right = propValue[1].Trim();
                        argsTable.Add(left, right);
                        LogEvent(THIS_MODULE, 110, EventType.Information, "ParseQueryString", CallingUser(context), "Added entry: " +
                            left + "=" + right);
                    }
                }

                ret = true;
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 105, EventType.Error, "ParseQueryString", CallingUser(context), "Error parsing query string: " + UrlArgs + ". Exception: " + ex.Message);

            }

            return ret;
        }

        /// <summary>
        ///  Posts back the request variables to Paypal.  Returns the response as a string.
        /// </summary>
        /// <param name="context">http context</param>
        /// <returns></returns>
      
        private string SendToPaypal(HttpContext context)
        {
            HttpRequest request = context.Request;

            // Step 1a: Modify the POST string.
            string formPostData = "cmd = _notify-validate";
            foreach (String key in request.Form)
            {
                string val = Encode(RequestValue(context, key));
                formPostData += string.Format("&{0}={1}", key, val);
            }

            // Step 1b: POST the data back to PayPal.
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            byte[] postByteArray = Encoding.ASCII.GetBytes(formPostData);

            LogEvent(THIS_MODULE, 80, EventType.Information, "SendToPaypal", CallingUser(context), formPostData);

            byte[] responseArray = client.UploadData(PaypalUrl, "POST", postByteArray);
            string response = Encoding.ASCII.GetString(responseArray);

            LogEvent(THIS_MODULE, 85, EventType.Information, "SendToPaypal", CallingUser(context), response);

            return response;
        }

        // Gets an activation key from the Qlm License Server
        private string SendToQlmWebService(HttpContext context, string vendor, string urlArgs)
        {
            HttpRequest request = context.Request;
            string transactionType = RequestValue(context, "txn_type");

            // Modify the POST string.
            string formPostData = String.Empty;
            foreach (String key in request.Form)
            {
                string val = Encode(RequestValue(context, key));
                formPostData += string.Format("&{0}={1}", key, val);
            }

            // Revert back to the real separator
            urlArgs = urlArgs.Replace('|', '&');

            string url = string.Empty;

            if (transactionType == "subscr_payment")
            {
                url = QlmWebUrl + "/RenewSubscriptionHttp?is_vendor=paypal" + "&is_qlmversion=" + QlmVersion + urlArgs;
            }
            else
            {
                //first subscription and non-subscription payments 
                url = QlmWebUrl + "/GetActivationKeyWithExpiryDate?is_vendor=paypal" + "&is_qlmversion=" + QlmVersion + urlArgs;
            }

            LogEvent(THIS_MODULE, 100, EventType.Information, "SendToQlmWeb", CallingUser(context), url);

            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            LogEvent(THIS_MODULE, 90, EventType.Information, "SendToQlmWeb", CallingUser(context), formPostData);


            byte[] postByteArray = Encoding.ASCII.GetBytes(formPostData);

            // Before calling the QLM Web Service, change to SSL
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;            
            byte[] responseArray = client.UploadData(url, "POST", postByteArray);
            // and then restore TLS for paypal

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string response = Encoding.ASCII.GetString(responseArray);

            LogEvent(THIS_MODULE, 95, EventType.Information, "SendToQlmWeb", CallingUser(context), response);

            return response;
        }

        /// <summary>
        /// Gets the value of a form field from a request
        /// </summary>
        /// <param name="context">http context</param>
        /// <param name="field">The field to retrieve</param>
        /// <returns></returns>
        private string RequestValue(HttpContext context, string field)
        {
            string val = string.Empty;

            try
            {
                val = context.Request.Form[field];
                if (val == null)
                {
                    val = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 60, EventType.Information, "RequestValue", CallingUser(context), ex.Message);
            }

            return val;
        }


        /// <summary>
        /// Applies an email template to the message
        /// </summary>
        /// <param name="templateFile">template filename</param>
        private string LoadTemplate(HttpContext context, string templateFile)
        {
            string doc = string.Empty;

            try
            {
                if (File.Exists(templateFile))
                {
                    using (FileStream fs = new FileStream(templateFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {

                        UTF8Encoding utf8 = new UTF8Encoding();
                        byte[] buffer = new byte[1000];
                        int bytesRead = 0;

                        while ((bytesRead = fs.Read(buffer, 0, 1000)) != 0)
                        {
                            doc += System.Text.Encoding.UTF8.GetString(buffer, 0, bytesRead);
                        }
                    }

                }
                else
                {
                    LogEvent(THIS_MODULE, 110, EventType.Error, "LoadTemplate", CallingUser(context), String.Format("The file {0} does not exist", templateFile));
                }

            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 105, EventType.Error, "LoadTemplate", CallingUser(context), ex.Message);
            }

            return doc;

        }

        private string ParseTemplate(HttpContext context, string template, out string ItemTemplate)
        {
            ItemTemplate = string.Empty;
            string main = string.Empty;
            bool readItemTemplate = false;

            try
            {
                using (StringReader sr = new StringReader(template))
                {
                    string line = null;

                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line == "%ItemTemplate%")
                        {
                            if (readItemTemplate)
                            {
                                readItemTemplate = false;
                            }
                            else
                            {
                                readItemTemplate = true;
                                //Have to add 1 placeholder line in the main file to hold location
                                main += line + "\n";
                            }
                        }
                        else
                        {
                            if (readItemTemplate)
                            {
                                ItemTemplate += line + "\n";
                            }
                            else
                            {
                                main += line + "\n";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 105, EventType.Error, "ParseTemplate", CallingUser(context), ex.Message);
            }

            return main;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="emailContent"></param>
        /// <param name="keys"></param>
        /// <param name="argsTable">The table of arguments relevant to this item</param>
        /// <returns></returns>
        private string ParseAndReplaceEmail(HttpContext context, string emailContent, string ItemTemplate, List<ItemData> Items)
        {
            string finalEmailContent = emailContent;

            try
            {

                HttpRequest request = context.Request;

                foreach (string paypalField in Settings.Default.paypalFields)
                {
                    if (!String.IsNullOrEmpty(paypalField))
                    {
                        string paypalVariable = "%" + paypalField + "%";
                        finalEmailContent = ReplaceString(finalEmailContent, paypalVariable, RequestValue(context, paypalField), StringComparison.CurrentCultureIgnoreCase);
                    }
                }


                finalEmailContent = ReplaceString(finalEmailContent, "%CompanyName%", CompanyName, StringComparison.CurrentCultureIgnoreCase);

                string finalItems = string.Empty;
                foreach (ItemData itm in Items)
                {
                    string thisItem = ItemTemplate;

                    foreach (string paypalField in Settings.Default.paypalFields)
                    {
                        if (!String.IsNullOrEmpty(paypalField))
                        {
                            string paypalVariable = "%" + paypalField + "%";
                            string newValue = RequestValue(context, paypalField + itm.ItemIndex);
                            if (string.IsNullOrEmpty(newValue))
                            {
                                //try with underscore
                                newValue = RequestValue(context, paypalField + "_" + itm.ItemIndex);

                                if (string.IsNullOrEmpty(newValue))
                                {
                                    //Try without the number
                                    newValue = RequestValue(context, paypalField);
                                }
                            }

                            //Make sure we found something useful
                            if (!string.IsNullOrEmpty(newValue))
                            { 
                                thisItem = ReplaceString(thisItem, paypalVariable, newValue, StringComparison.CurrentCultureIgnoreCase); 
                            }
                        }
                    }

                    thisItem = ReplaceString(thisItem, "%keys%", itm.ActivationKeys, StringComparison.CurrentCultureIgnoreCase);

                    //indicates single item, may not have number
                    string productName = itm.ItemName;

                    if (!String.IsNullOrEmpty(productName))
                    {
                        thisItem = ReplaceString(thisItem, "%ProductName%", productName, StringComparison.CurrentCultureIgnoreCase);
                        thisItem = ReplaceString(thisItem, "%MajorVersion%", itm.ArgsTable["is_majorversion"].ToString(), StringComparison.CurrentCultureIgnoreCase);
                        thisItem = ReplaceString(thisItem, "%MinorVersion%", itm.ArgsTable["is_minorversion"].ToString(), StringComparison.CurrentCultureIgnoreCase);
                    }
                    else
                    {
                        LogEvent(THIS_MODULE, 145, EventType.Error, "ParseAndReplaceEmail", CallingUser(context), "Product name was not specified in item_name nor item_name1.");
                    }


                    finalItems += thisItem;
                }

                finalEmailContent = ReplaceString(finalEmailContent, "%ItemTemplate%", finalItems, StringComparison.CurrentCultureIgnoreCase);
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 125, EventType.Error, "ParseAndReplaceEmail", CallingUser(context), ex.Message);
            }

            return finalEmailContent;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="keys"></param>
        /// <param name="argsTable">The table of arguments relevant to this item</param>
        /// <returns></returns>
        private string ApplyEmailTemplate(HttpContext context, List<ItemData> Items, out bool isHtmlFormat)
        {
            isHtmlFormat = Settings.Default.htmlFormat;

            HttpRequest request = context.Request;
            string emailTemplate = Settings.Default.templateFile;

            foreach (ItemData itm in Items)
            {
                if (itm.ArgsTable.ContainsKey("is_emailtemplate"))
                {
                    emailTemplate = itm.ArgsTable["is_emailtemplate"].ToString();
                }
            }

            string ext = Path.GetExtension(emailTemplate);
            if ((String.Compare(ext, ".html", true) == 0) ||
                (String.Compare(ext, ".htm", true) == 0))
            {
                isHtmlFormat = true;
            }

            string emailTemplateText = LoadTemplate(context, Path.Combine(Path.GetDirectoryName(request.PhysicalPath), emailTemplate));
            string itemTemplate = string.Empty;
            string emailContent = ParseTemplate(context, emailTemplateText, out itemTemplate);
            return ParseAndReplaceEmail(context, emailContent, itemTemplate, Items);

        }

        // Email the customer their keys
        private void SendCustomerActivationKey(HttpContext context, List<ItemData> Items)
        {
            HttpRequest request = context.Request;

            string payer_email = RequestValue(context, "payer_email");
            string receiver_email = RequestValue(context, "receiver_email");
            bool isHtmlFormat = false;

            string body = ApplyEmailTemplate(context, Items, out isHtmlFormat);

            LogEvent(THIS_MODULE, 56, EventType.Information, "SendCustomerActivationKey", CallingUser(context), body);

            SendMail(context,
                     receiver_email, payer_email,
                     String.Format (Settings.Default.emailSubject, CompanyName),
                     body);
        }

        private void LogEvent(string source, int eventID, EventType type,
                               string category, string user, string details)
        {
            try
            {
                int nType = (int)type;
                if ((loggingLevel & nType) == nType)
                {

                    EventLogTableAdapter ta = new EventLogTableAdapter();
                    //ta.Connection = new System.Data.OleDb.OleDbConnection();

                    ta.AddEvent(source, eventID, (short)type, DateTime.UtcNow, category, user, details.Trim());

                }
                else
                {
                }
            }
            catch
            {
            }
        }

        //
        // Fast String Replace
        //
        private string ReplaceString(string s, string oldValue, string newValue, StringComparison comparisonType)
        {
            try
            {
                if (s == null)
                {
                    return null;
                }

                if (String.IsNullOrEmpty(oldValue))
                {
                    return s;
                }

                int lenOldValue = oldValue.Length;
                int curPosition = 0;
                int idxNext = 0;

                StringBuilder result = new StringBuilder();
                idxNext = s.IndexOf(oldValue, comparisonType);

                while (idxNext >= 0)
                {
                    result.Append(s, curPosition, idxNext - curPosition);
                    result.Append(newValue);
                    curPosition = idxNext + lenOldValue;
                    idxNext = s.IndexOf(oldValue, curPosition, comparisonType);
                }

                result.Append(s, curPosition, s.Length - curPosition);

                return result.ToString();
            }
            catch (Exception ex)
            {
                LogEvent(THIS_MODULE, 150, EventType.Information, "ReplaceString", string.Empty, ex.Message);
            }

            return s;
        }

        /// <summary>
        /// Gets the identity of the current user and returns it's string value, or host address if not authenticated?
        /// </summary>
        /// <param name="context">http context to check.</param>
        /// <returns></returns>
        private string CallingUser(HttpContext context)
        {
            string user = string.Empty;

            if (context != null)
            {
                user = HttpContext.Current.User.Identity.Name;

                if (String.IsNullOrEmpty(user))
                {
                    user = HttpContext.Current.Request.UserHostAddress;
                }
            }

            return user;
        }

        private string GetProductArgument(HttpContext context, int index)
        {
            string customArg = string.Empty;

            if (!String.IsNullOrEmpty(Settings.Default.qlmArgsFieldName))
            {
                string arg = RequestValue(context, Settings.Default.qlmArgsFieldName);

                if (!String.IsNullOrEmpty(arg))
                {
                    if (IsQlmArgument(arg))
                    {
                        customArg += arg;
                    }
                }
            }

            if (Settings.Default.ignoreCustomArgument == false)
            {
                string arg = RequestValue(context, "custom");

                if (!String.IsNullOrEmpty(arg))
                {
                    if (IsQlmArgument (arg))
                    {
                        customArg += arg;
                    }
                }
            }

            if (Settings.Default.ignoreItemNumberArgument == false)
            {
                if (index > 1)
                {
                    string arg = RequestValue(context, "item_number" + index.ToString());
                    if (IsQlmArgument(arg))
                    {
                        customArg += arg;
                    }
                }
                else
                {
                    string arg = RequestValue(context, "item_number");
                    if (String.IsNullOrEmpty (arg))
                    {
                        arg = RequestValue(context, "item_number1");
                    }

                    if (IsQlmArgument(arg))
                    {
                        customArg += arg;
                    }
                }
            }

            return customArg;

        }

        private bool IsQlmArgument(string arg)
        {
            if (String.IsNullOrEmpty(arg))
            {
                return false;
            }
            return arg.ToLower().Contains("is_productid");
        }

        private void ProcessItem(HttpContext context, OrderInfo orderInfo, ItemData itm)
        {
            
            //Parse the string (constructs a hash table of values for this item which can then be passed to emails, etc.)
            Hashtable argsTable;
            ParseQueryString(context, itm.UrlArgs, out argsTable);
            itm.ArgsTable = argsTable;
            string response = string.Empty;
            string subscr_id = RequestValue(context, "subscr_id");// it will be there in case of subscriptions

            // if it is new subscription
            if ((orderInfo.TransactionType == "subscr_signup") && (subscr_id != string.Empty) )
            {
                LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", orderInfo.CallingUser, "New Subscriber: " + subscr_id);

                DateTime subscrDate = DateTime.Now;
                if(! TryParsePaypalDatetimeToLocal(RequestValue(context, "subscr_date"), out subscrDate))
                {
                    //Either subscription date not found or we are unable to parse it
                    SendMail(context,
                               orderInfo.Business, orderInfo.PayerEmail,
                               "Your purchase with " + CompanyName,
                               "Cannot retrieve your license key. Please contact " + CompanyName + "at " + SupportEmail + ".");

                    LogEvent(THIS_MODULE, 45, EventType.Error, "ProcessTransaction", orderInfo.CallingUser, "Exception: Subscription date not found.");
                    return;
                }

                string paypalDuration = RequestValue(context, "period3");
                string expDuration = GetExpDuration(paypalDuration).ToString();

                itm.UrlArgs = itm.UrlArgs + "&is_subscr_id=" + subscr_id + "&is_expduration=" + expDuration;
                response = SendToQlmWebService(context, "paypal", itm.UrlArgs);

            }
            else
            {
                // if the order is completed, get an activation key from QlmWeb
                LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", orderInfo.CallingUser, "Payment Status: " + orderInfo.PaymentStatus);

                if ((orderInfo.TransactionType == "subscr_payment") && (subscr_id != string.Empty))
                {
                    //this is subscriber payment and payment status is already completed if we are here.
                    LogEvent(THIS_MODULE, 35, EventType.Information, "ProcessTransaction", orderInfo.CallingUser, "Subscriber Payment: " + subscr_id);

                    itm.UrlArgs = itm.UrlArgs + "&is_subscr_id=" + subscr_id;
                }
                response = SendToQlmWebService(context, "paypal", itm.UrlArgs);
            }


           

                // parse the activation key
                string keys = string.Empty;
                XmlDocument xmlDoc = new XmlDocument();

                try
                {
                    xmlDoc.LoadXml(response);

                    System.Xml.XmlNodeList nodes = xmlDoc.GetElementsByTagName("Key");
                    if (nodes.Count != 0)
                    {
                        foreach (System.Xml.XmlNode node in nodes)
                        {
                            keys += node.InnerText + "\n";
                        }
                    }else
                    {
                        nodes = xmlDoc.GetElementsByTagName("GetActivationKey");
                        if (nodes.Count != 0)
                        {
                            foreach (System.Xml.XmlNode node in nodes)
                            {
                                keys += node.InnerText + "\n";
                            }
                        }
                    }
                }
                catch (System.Exception e)
                {
                    SendMail(context,
                               orderInfo.Business, orderInfo.PayerEmail,
                               "Your purchase with " + CompanyName,
                               "Cannot retrieve your license key. Please contact " + CompanyName + "at " + SupportEmail + ".");

                    ReportException(context, e);
                    LogEvent(THIS_MODULE, 45, EventType.Error, "ProcessTransaction", orderInfo.CallingUser, "Exception: " + e.Message);
                    return;
                }

                //Collect the data to be put in the email
                itm.ActivationKeys = keys;
            }

        private string SendToPaypal_Test(HttpContext context)
        {
            HttpRequest request = context.Request;
            string response = string.Empty;

            // Step 1a: Modify the POST string.
            string formPostData = "cmd = _notify-validate";
            foreach (String key in request.Form)
            {
                string val = Encode(RequestValue(context, key));
                formPostData += string.Format("&{0}={1}", key, val);
            }

            byte[] postByteArray = Encoding.ASCII.GetBytes(formPostData);

            LogEvent(THIS_MODULE, 80, EventType.Information, "SendToPaypal", CallingUser(context), formPostData);

            HttpWebRequest payPalRequest = (HttpWebRequest)WebRequest.Create(PaypalUrl);
            payPalRequest.ProtocolVersion = HttpVersion.Version10;
            payPalRequest.Method = "POST";
            //payPalRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            payPalRequest.ContentType = "application/x-www-form-urlencoded";
            payPalRequest.ContentLength = postByteArray.Length;
            Stream reqStream = payPalRequest.GetRequestStream();
            reqStream.Write(postByteArray, 0, postByteArray.Length);
            reqStream.Close();

            try
            {
                HttpWebResponse payPalResponse = (HttpWebResponse)payPalRequest.GetResponse();

                using (StreamReader sr = new StreamReader(payPalResponse.GetResponseStream()))
                {
                    response = sr.ReadToEnd();
                    LogEvent(THIS_MODULE, 85, EventType.Information, "SendToPaypal", CallingUser(context), response);
                }
            }
            catch (WebException ex)
            {

            }

            return response;
        }
        #endregion

        #region Properties

        public string PaypalUrl
        {
            get
            {
                return paypalUrl;

            }
            set
            {
                paypalUrl = value;
            }
        }


        public string QlmWebUrl
        {
            get
            {
                return qlmWebUrl;

            }
            set
            {
                qlmWebUrl = value;
            }
        }

        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
            }
        }

        public string SupportEmail
        {
            get
            {
                return supportEmail;
            }
            set
            {
                supportEmail = value;
            }
        }



        public string QlmVersion
        {
            get
            {
                return qlmVersion;
            }
            set
            {
                qlmVersion = value;
            }
        }
        #endregion

        #region Server Properties
        private string GetServerProperty(string property, string configValue)
        {
            string value = configValue;

            if (serverPropertiesDataTable != null)
            {
                try
                {
                    string filter = String.Format("PropertyName='{0}'", property);
                    ServerPropertiesDs.ServerPropertiesRow[] dr = (ServerPropertiesDs.ServerPropertiesRow[])serverPropertiesDataTable.Select(filter);
                    if (dr.Length > 0)
                    {
                        value = dr[0].PropertyValue;
                        LogEvent(THIS_MODULE, 230, EventType.Information, "GetServerProperty", string.Empty, String.Format("Found value {0} for property {1}", value, property));
                    }
                    else
                    {
                        LogEvent(THIS_MODULE, 190, EventType.Error, "GetServerProperty", string.Empty, String.Format("No value found for property {0}, Filter={1}", property, filter));
                    }
                }
                catch (Exception ex)
                {
                    LogEvent(THIS_MODULE, 205, EventType.Error, "GetServerProperty", string.Empty, ex.Message);
                }
            }
            else
            {
                LogEvent(THIS_MODULE, 175, EventType.Error, "GetServerProperty", string.Empty, "ServerProperties table is null");
            }

            return value;

        }

        private bool GetServerProperty(string property, bool configValue)
        {
            bool value = configValue;

            if (serverPropertiesDataTable != null)
            {
                try
                {
                    string filter = String.Format("PropertyName='{0}'", property);
                    ServerPropertiesDs.ServerPropertiesRow[] dr = (ServerPropertiesDs.ServerPropertiesRow[])serverPropertiesDataTable.Select(filter);
                    if (dr.Length > 0)
                    {
                        value = Convert.ToBoolean(dr[0].PropertyValue);
                        LogEvent(THIS_MODULE, 235, EventType.Information, "GetServerProperty", string.Empty, String.Format("Found value {0} for property {1}", value, property));
                    }
                    else
                    {
                        LogEvent(THIS_MODULE, 195, EventType.Error, "GetServerProperty", string.Empty, String.Format("No value found for property {0}, Filter={1}", property, filter));
                    }
                }
                catch (Exception ex)
                {
                    LogEvent(THIS_MODULE, 210, EventType.Error, "GetServerProperty", string.Empty, ex.Message);
                }
            }
            else
            {
                LogEvent(THIS_MODULE, 180, EventType.Error, "GetServerProperty", string.Empty, "ServerProperties table is null");
            }

            return value;

        }

        private int GetServerProperty(string property, int configValue)
        {
            int value = configValue;

            if (serverPropertiesDataTable != null)
            {
                try
                {
                    string filter = String.Format("PropertyName='{0}'", property);
                    ServerPropertiesDs.ServerPropertiesRow[] dr = (ServerPropertiesDs.ServerPropertiesRow[])serverPropertiesDataTable.Select(filter);
                    if (dr.Length > 0)
                    {
                        value = Convert.ToInt32(dr[0].PropertyValue);
                        LogEvent(THIS_MODULE, 240, EventType.Information, "GetServerProperty", string.Empty, String.Format("Found value {0} for property {1}", value, property));
                    }
                    else
                    {
                        LogEvent(THIS_MODULE, 200, EventType.Error, "GetServerProperty", string.Empty, String.Format("No value found for property {0}, Filter={1}", property, filter));
                    }
                }
                catch (Exception ex)
                {
                    LogEvent(THIS_MODULE, 215, EventType.Error, "GetServerProperty", string.Empty, ex.Message);
                }
            }
            else
            {
                LogEvent(THIS_MODULE, 185, EventType.Error, "GetServerProperty", string.Empty, "ServerProperties table is null");
            }

            return value;

        }

        private void InitServerProperties()
        {
            if (refreshServerProperties)
            {
                try
                {
                    ServerPropertiesTableAdapter ta = new ServerPropertiesTableAdapter();

                    serverPropertiesDataTable = ta.GetData();
                    LogEvent(THIS_MODULE, 225, EventType.Information, "InitServerProperties", string.Empty, 
                                String.Format ("InitServerProperties done. Loaded {0} properties", serverPropertiesDataTable.Rows.Count));

                }
                catch (Exception ex)
                {
                    LogEvent(THIS_MODULE, 170, EventType.Error, "InitServerProperties", string.Empty, ex.Message);
                }
            }
            
        }

        /// <summary>
        /// Tries to parse PayPal formatted date and time and converts it to an UTC.
        /// </summary>
        private bool TryParsePaypalDatetimeToLocal(string paypalDatetime, out DateTime retValue)
        {
            DateTime paymentDate;

            // PayPal formats from docs
            string[] formats = new string[] { "HH:mm:ss dd MMM yyyy PDT", "HH:mm:ss dd MMM yyyy PST",
                                      "HH:mm:ss dd MMM, yyyy PST", "HH:mm:ss dd MMM, yyyy PDT",
                                      "HH:mm:ss MMM dd, yyyy PST", "HH:mm:ss MMM dd, yyyy PDT" };
            if (false == DateTime.TryParseExact(paypalDatetime, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out paymentDate))
            {
                retValue = DateTime.MinValue;
                return false;
            }

            DateTime dtUtc = TimeZoneInfo.ConvertTimeToUtc(paymentDate, TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));

            TimeZone localZone = TimeZone.CurrentTimeZone;
            retValue = localZone.ToLocalTime(dtUtc);

            return true;
        }
        
        private int GetExpDuration(string duration)
        {
            //duration is period3 e.g 1 D
            duration = duration.Trim();
            if (duration.Length != 3) return 0;

            int days = Convert.ToInt16(duration.Substring(0, 1));

            string period = duration.Substring(2);

            switch (period.ToUpper())
            {
                case "D":
                    return days;
                    break;

                case "W":
                    return days * 7;
                    break;

                case "M":
                    return days * 30;
                    break;

                case "Y":
                    return days * 365;
                    break;
            }

            return 0;
        }

        private string GetExpDate(DateTime start, int duration)
        {
            DateTime exp = start.AddDays(duration);
            return exp.ToString("yyyy-MM-dd");

        }

        #endregion

        protected class ItemData
        {
            public string UrlArgs { get; set; }
            public string ActivationKeys { get; set; }
            public string ItemName;
            public Hashtable ArgsTable { get; set; }
            public int ItemIndex { get; set; }

            public ItemData() { }
            public ItemData(string args, string itemName)
            {
                UrlArgs = args;
                ItemName = itemName;
            }
            public ItemData(string args, int indx, string itemName)
            {
                UrlArgs = args;
                ItemIndex = indx;
                ItemName = itemName;
            }
        }

        protected class OrderInfo
        {
            public string Business;
            public string PayerEmail;
            public string CallingUser;
            public string PaymentStatus;
            public string TransactionType;

        }

        

    }
}
